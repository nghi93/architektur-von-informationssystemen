package to.mps.auftragskomponente.dataaccesslayer;

import static javax.persistence.GenerationType.SEQUENCE;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import to.mps.common.AbstractEntity;
import to.mps.fertigungskomponente.dataaccesslayer.Bauteil;
import to.mps.fertigungskomponente.dataaccesslayer.Fertigungsauftrag;



@Entity
@Table(name="Auftrag")
public class Auftrag extends AbstractEntity{

	private boolean istAbgeschlossen;
	private Date beauftragtAm;
	private Bauteil bauteil;
	
	public Auftrag(){
		
	}
	
	public Auftrag(boolean istAbgeschlossen, Date beauftragtAm, Bauteil bauteil){
		super();
		this.setIstAbgeschlossen(istAbgeschlossen);
		this.setBeauftragtAm(beauftragtAm);
		this.setBauteil(bauteil);
	}
	

	@Column
	public boolean isIstAbgeschlossen() {
		return istAbgeschlossen;
	}

	public void setIstAbgeschlossen(boolean istAbgeschlossen) {
		this.istAbgeschlossen = istAbgeschlossen;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	public Date getBeauftragtAm() {
		return beauftragtAm;
	}

	public void setBeauftragtAm(Date beauftragtAm) {
		this.beauftragtAm = beauftragtAm;
	}
	
	@ManyToOne
	@JoinColumn(name="BAUTEIL_ID")
	public Bauteil getBauteil() {
		return bauteil;
	}

	public void setBauteil(Bauteil bauteil) {
		this.bauteil = bauteil;
	}	
}
