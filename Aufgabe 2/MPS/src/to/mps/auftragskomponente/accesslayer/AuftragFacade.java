package to.mps.auftragskomponente.accesslayer;

import to.mps.auftragskomponente.dataaccesslayer.Auftrag;
import to.mps.auftragskomponente.dataaccesslayer.AuftragRepo;


public class AuftragFacade implements AuftragServices{
	
	private AuftragRepo auftragRepo;
	
	public AuftragFacade(){
		auftragRepo = new AuftragRepo();
	}
	
	@Override
	public Auftrag erstelleAuftrag(Auftrag a) {
		auftragRepo.saveAuftrag(a);
		return a;
	}
}
