package to.mps.auftragskomponente.accesslayer;

import to.mps.auftragskomponente.dataaccesslayer.Auftrag;

public interface AuftragServices {
	public Auftrag erstelleAuftrag(Auftrag a);
}
