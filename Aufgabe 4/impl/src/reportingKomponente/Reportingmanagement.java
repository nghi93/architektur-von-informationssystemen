package reportingKomponente;

import java.util.Date;

public class Reportingmanagement implements IReporting{
	private Reportingverwalter reportingverwalter;
	private KibanaAdapter kibana;
	
	public Reportingmanagement(Reportingverwalter verwalter, KibanaAdapter kibana){
		this.reportingverwalter = verwalter;
	}
	
	@Override
	public void logKunde(Date date) {
		String send = reportingverwalter.loadKunde(date);
		kibana.send(send);
	}

	@Override
	public void logAngebote(Date date) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void logAuftrag(Date date) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void logZeitdauerAuftragZahlungseingang(Date tag) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void logZeitdauerFertigstellungProduktAusliferung(Date tag) {
		// TODO Auto-generated method stub
		
	}

}
