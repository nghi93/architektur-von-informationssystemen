package reportingKomponente;

import java.util.Date;

public interface IReporting {
	public void logKunde(Date tag);
	public void logAngebote(Date tag);
	public void logAuftrag(Date tag);
	public void logZeitdauerAuftragZahlungseingang(Date tag);
	public void logZeitdauerFertigstellungProduktAusliferung(Date tag);
}
