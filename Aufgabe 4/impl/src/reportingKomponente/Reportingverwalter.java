package reportingKomponente;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.google.gson.Gson;

import models.Angebot;
import utils.HibernateMaster;

/**
 * returns json-objects as string
 */
public class Reportingverwalter {
	private Gson gson = new Gson();
	public String loadAngebot(Date date){

		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.set(Calendar.DATE, c.getActualMaximum(Calendar.DATE));
		Date end = c.getTime();
		@SuppressWarnings("unchecked")
		List<Angebot> entities = HibernateMaster.execute("SELECT * FROM ANGEBOT WHERE created > :start and created < :end", Angebot.class, "start", date.toString(), "end", end.toString());
		String result = "";
		for(Angebot a : entities){
			result+=gson.toJson(a);
		}
		
		return result;
	}
	
	public String loadAuftrag(Date date){
		return "";
	}
	
	public String loadKunde(Date date){
		return "";
	}
	
	public String loadZeitdauerAuftragZahlung(Date date){
		return "";
	}
	
	public String loadZeitdauerFertigstellungAusliferung(Date date){
		return "";
	}
}
