package kundenkomponente;

import static utils.HibernateMaster.loadObject;
import static utils.HibernateMaster.persistObject;
import models.Kunde;

public class Kundenverwalter {
	public Kunde erstelleKunde(String Name, String Adresse) {
		Kunde temp = new Kunde(Name, Adresse);
		persistObject(temp);
		return temp;
	}
	
	public Kunde getKunde(int KundenNr){
		Kunde kunde = (Kunde) loadObject(Kunde.class, KundenNr);
		return kunde;
	}
}
