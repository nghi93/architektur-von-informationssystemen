package kundenkomponente;

public class Kundenmanagement implements IKunde {
	
	private Kundenverwalter kundenverwalter;
	
	public Kundenmanagement(Kundenverwalter kundenverwalter) {
		this.kundenverwalter = kundenverwalter;
	}

	@Override
	public void erstelleKunde(String Name, String Adresse) {
		kundenverwalter.erstelleKunde(Name, Adresse);
	}

	@Override
	public void getKunde(int KundenNr) {
		kundenverwalter.getKunde(KundenNr);
	}

}
