package models;
import java.util.Date;

import javax.persistence.*;
@Entity
@Table(name = "Kundenauftrag")
public class Kundenauftrag {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int kundenAuftragId;
	@OneToOne
	@JoinColumn(name="angebotId")
	private Angebot angebot = null;
	
	@Temporal(TemporalType.DATE)
	private Date created = new Date();

	@Temporal(TemporalType.DATE)
	private Date updated = new Date();

	@PreUpdate
	public void setLastUpdate() {  this.updated = new Date(); }
	
	@Column
	private boolean isStoniert = false;
	
	public boolean isStoniert() {
		return isStoniert;
	}
	
	public void storniere() {
		this.isStoniert = true;
	}
	
	public Kundenauftrag(Angebot angebot){
		this.angebot = angebot;
		this.angebot.setKundenauftrag(this);
	}
	public Kundenauftrag(){}
	
	//### GETTER ###
	public int getKundenauftragNr(){
		return this.kundenAuftragId;
	}
	
	
}
